import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGaurdService } from './services/gaurds/auth-gaurd.service';
import { SigninGaurdService } from './services/gaurds/signin-gaurd.service'

const routes: Routes = [
  { path: "", loadChildren: "./home/home.module#HomePageModule" },
  { path: "about", loadChildren: "./about/about.module#AboutPageModule" },
  {
    path: "contact",
    loadChildren: "./contact/contact.module#ContactPageModule"
  },
  {
    path: "my-account",
    loadChildren: "./my-account/my-account.module#MyAccountPageModule",
    canActivate: [AuthGaurdService]
  },
  { path: "home", loadChildren: "./home/home.module#HomePageModule" },
  {
    path: "login",
    loadChildren: "./login/login.module#LoginPageModule",
    canActivate: [SigninGaurdService]
  },
  {
    path: "add-account",
    loadChildren: "./add-account/add-account.module#AddAccountPageModule",
    canActivate: [SigninGaurdService]
  },
  { path: "terms", loadChildren: "./terms/terms.module#TermsPageModule" },
  {
    path: "privacy",
    loadChildren: "./privacy/privacy.module#PrivacyPageModule"
  },
  {
    path: "product/:id",
    loadChildren: "./product/product.module#ProductPageModule"
  },
  { path: "cart", loadChildren: "./cart/cart.module#CartPageModule" },
  {
    path: "checkout",
    loadChildren: "./checkout/checkout.module#CheckoutPageModule",
    canActivate: [AuthGaurdService]
  },
  {
    path: "checkout-devivery",
    loadChildren:
      "./checkout-devivery/checkout-devivery.module#CheckoutDeviveryPageModule"
  },
  {
    path: "checkout-payment",
    loadChildren:
      "./checkout-payment/checkout-payment.module#CheckoutPaymentPageModule"
  },
  {
    path: "products",
    loadChildren: "./products/products.module#ProductsPageModule"
  },
  { path: "search", loadChildren: "./search/search.module#SearchPageModule" },
  {
    path: "my-orders",
    loadChildren: "./my-orders/my-orders.module#MyOrdersPageModule",
    canActivate: [AuthGaurdService]
  },
  {
    path: "categories",
    loadChildren: "./categories/categories.module#CategoriesPageModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
