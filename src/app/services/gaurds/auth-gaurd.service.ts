import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { map } from 'rxjs/operators'
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGaurdService implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  constructor( private router: Router, private afAuth: AngularFireAuth) { }

  canActivate(){
    return this.afAuth.authState.pipe(map(auth => {
      if(!auth){
        this.router.navigate(['/login']);
        return false;
      } else {
        return true;
      }
    }))
  }

}
