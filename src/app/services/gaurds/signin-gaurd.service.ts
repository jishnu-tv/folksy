import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { map } from 'rxjs/operators'
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class SigninGaurdService implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;

  constructor( private afAuth: AngularFireAuth, private router: Router) { }

  canActivate(){
    return this.afAuth.authState.pipe(map(auth => {
      if(!auth){
        return true;
      } else {
        this.router.navigate(['/']);
        return false;
      }
    }))
  }
}
