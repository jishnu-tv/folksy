import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators'
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private afAuth: AngularFireAuth ) { }

   get getAuth(){
    return this.afAuth.authState.pipe(map( auth => auth ));
  }
}
