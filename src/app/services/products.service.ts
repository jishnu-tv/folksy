import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Product } from "../models/Product";
import { Homeoffer } from "../models/Homeoffers";

@Injectable({
  providedIn: "root"
})
export class ProductsService {
  productsCollection: AngularFirestoreCollection<Product>;
  productDoc: AngularFirestoreDocument<Product>;
  products: Observable<Product[]>;
  product: Observable<Product>;

  offerDoc: AngularFirestoreDocument<Homeoffer>;
  home_offers: Observable<Homeoffer[]>;
  home_offer: Observable<Homeoffer>;

  constructor(private afs: AngularFirestore) {}

  getProducts() {
    this.products = this.afs
      .collection("products")
      .snapshotChanges()
      .pipe(
        map(products => {
          return products.map(product => {
            const data = product.payload.doc.data() as Product;
            const id = product.payload.doc.id;
            return { id, ...data };
          });
        })
      );
    return this.products;
  }

  getOffers() {
    this.home_offers = this.afs
      .collection("offers_home")
      .snapshotChanges()
      .pipe(
        map(home_offers => {
          return home_offers.map(home_offer => {
            const data = home_offer.payload.doc.data() as Homeoffer;
            const id = home_offer.payload.doc.id;
            return { id, ...data };
          });
        })
      );
    return this.home_offers;
  }

  getProduct(id: string) {
    this.productDoc = this.afs.doc<Product>("products/" + id);
    return (this.product = this.productDoc.valueChanges());
  }

  getOffer(id: string) {
    this.offerDoc = this.afs.doc<Homeoffer>("offers_home/" + id);
    return (this.home_offer = this.offerDoc.valueChanges());
  }
}
