import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CheckoutDeviveryPage } from './checkout-devivery.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutDeviveryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CheckoutDeviveryPage]
})
export class CheckoutDeviveryPageModule {}
