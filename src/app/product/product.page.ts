import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { Product } from '../models/Product';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  item: Product;
  id: string = '';
  stock: string = '';
  highlight: Array<any>;

  constructor(
    private productsService: ProductsService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    public actionSheetController: ActionSheetController
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.productsService.getProduct(this.id).subscribe(product => {

      // Stock
      if (product.stock < 50 && product.stock >= 10) {
        this.stock = "Few left"
      } else if (product.stock > 50) {
        this.stock = "In stock"
      } else if (product.stock === 0) {
        this.stock = "Out of stock"
      } else if (product.stock < 10 && product.stock >= 1) {
        this.stock = "Only " + product.stock + " left"
      }

      // Highlights
      this.highlight = product.highlights.split(/\n/);

      this.item = product;
    })
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Share',
      buttons: [{
        text: 'Whatsapp',
        icon: 'logo-whatsapp',
        handler: () => {
          console.log('Delete clicked');
        }
      }, {
        text: 'Facebook',
        icon: 'logo-facebook',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Email',
        icon: 'mail',
        handler: () => {
          console.log('Play clicked');
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  back() {
    this.navCtrl.navigateBack;
  }
}
