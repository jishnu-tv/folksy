import { Component, OnInit } from "@angular/core";
import { ProductsService } from "../services/products.service";
import { Product } from "../models/Product";

@Component({
  selector: "app-products",
  templateUrl: "./products.page.html",
  styleUrls: ["./products.page.scss"]
})
export class ProductsPage implements OnInit {
  products: Product[];
  page: string = "";

  constructor(private productsService: ProductsService) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.productsService.getProducts().subscribe(data => {
      this.products = data;
    });
  }

  doRefresh(event) {
    console.log("Begin async operation");

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
    }, 2000);
  }
}
