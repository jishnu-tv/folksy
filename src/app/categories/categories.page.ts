import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-categories",
  templateUrl: "./categories.page.html",
  styleUrls: ["./categories.page.scss"]
})
export class CategoriesPage implements OnInit {
  categories = [
    {
      title: "Electronics",
      bg: "gradient-bg-1"
    },
    {
      title: "Fasion",
      bg: "gradient-bg-2"
    },
    {
      title: "Lighting",
      bg: "gradient-bg-1"
    },
    {
      title: "Kitchen",
      bg: "gradient-bg-3"
    },
    {
      title: "Home",
      bg: "gradient-bg-4"
    },
    {
      title: "Footware",
      bg: "gradient-bg-3"
    },
    {
      title: "Vegitables",
      bg: "gradient-bg-2"
    },
    {
      title: "Frutes",
      bg: "gradient-bg-1"
    }
  ];

  constructor() {}

  ngOnInit() {}
}
