import { Component } from "@angular/core";
import * as firebase from "firebase";
import { Router } from "@angular/router";
import { ProductsService } from "../services/products.service";
import { Product } from "../models/Product";
import { Homeoffer } from "../models/Homeoffers";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  products: Product[];
  offers: Homeoffer[];
  page: string = "";

  constructor(
    private router: Router,
    private productsService: ProductsService
  ) {}

  ionViewWillEnter() {
    this.productsService.getProducts().subscribe(data => {
      this.products = data;
    });

    this.productsService.getOffers().subscribe(offer => {
      this.offers = offer;
    });
  }

  signOut() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("You are now logged out");
        this.router.navigate(["/login"]);
      })
      .catch(error => {
        console.log(error);
      });
  }
}
