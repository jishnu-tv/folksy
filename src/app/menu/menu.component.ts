import { Component, OnInit } from "@angular/core";
import * as firebase from "firebase";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  authStatus: any;

  pages = [
    {
      title: "Home",
      url: "/"
    },
    {
      title: "Account",
      url: "/my-account"
    },
    {
      title: "Products",
      url: "/products"
    },
    {
      title: "Categories",
      url: "/categories"
    },
    {
      title: "About",
      url: "/about"
    },
    {
      title: "Contact",
      url: "/contact"
    }
  ];

  isLoggedIn: boolean;

  constructor(private router: Router, private authService: AuthService) {}

  signOut() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        console.log("You are now signed out");
        this.router.navigate(["/login"]);
      })
      .catch(error => {
        console.log(error);
      });
  }

  ngOnInit() {
    this.authService.getAuth.subscribe(auth => {
      if (auth) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }
}
