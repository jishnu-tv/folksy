// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB7dukhVFwd66zIW5nb8CckQHWFgYR52Sk",
    authDomain: "folksy-89b43.firebaseapp.com",
    databaseURL: "https://folksy-89b43.firebaseio.com",
    projectId: "folksy-89b43",
    storageBucket: "folksy-89b43.appspot.com",
    messagingSenderId: "977755733418",
    appId: "1:977755733418:web:49ada999d289e099"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
